package com.repoInterface;

public interface InMemoryRepository<T> {
    void add(T obj);

    boolean contains(T obj);

    void remove(T obj);
}
