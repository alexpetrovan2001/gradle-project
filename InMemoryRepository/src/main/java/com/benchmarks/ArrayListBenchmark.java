package com.benchmarks;

import com.repoImplementation.ArrayListBasedRepo;
import com.repoInterface.InMemoryRepository;
import com.repoModel.Order;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1)
@Measurement(iterations = 20, time = 1)
@Fork(1)
@State(Scope.Benchmark)
public class ArrayListBenchmark {

    @Param({"20"})
    public int size;

    private InMemoryRepository<Order> repo;

    @Setup(Level.Trial)
    public void setup() {
        repo = new ArrayListBasedRepo<>();
    }

    @Benchmark
    public void testCreate(Blackhole consumer) {
        IntStream.rangeClosed(1, size)
                .forEach(i -> repo.add(new Order(i, i * 10, i * 2)));
        consumer.consume(repo);
    }

    @Benchmark
    public void testContains(Blackhole consumer) {
        IntStream.rangeClosed(1, size)
                .forEach(i -> repo.add(new Order(i, i * 10, i * 2)));
        consumer.consume(repo.contains(new Order(size, size * 10, size * 2)));
    }

    @Benchmark
    public void testRemove(Blackhole consumer) {
        IntStream.rangeClosed(1, size)
                .forEach(i -> repo.add(new Order(i, i * 10, i * 2)));
        Order orderToRemove = new Order(size, size * 10, size * 2);
        repo.add(orderToRemove);
        consumer.consume(repo);
        repo.remove(orderToRemove);
    }
}
