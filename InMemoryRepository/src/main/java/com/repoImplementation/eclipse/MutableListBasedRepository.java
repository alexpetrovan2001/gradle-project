package com.repoImplementation.eclipse;

import com.repoInterface.InMemoryRepository;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

public class MutableListBasedRepository<T> implements InMemoryRepository<T> {
    private final MutableList<T> mutableList;

    public MutableListBasedRepository(){
        this.mutableList = FastList.newList();
    }

    @Override
    public void add(T obj) {
        this.mutableList.add(obj);
    }

    @Override
    public boolean contains(T obj) {
        return this.mutableList.contains(obj);
    }

    @Override
    public void remove(T obj) {
        this.mutableList.remove(obj);
    }
}
