package com.repoImplementation;

import com.repoInterface.InMemoryRepository;
import com.repoModel.Model;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepo<T extends Model> implements InMemoryRepository<T> {
    private final ConcurrentHashMap<Integer, T> concurrentHashMap;

    public ConcurrentHashMapBasedRepo(){
        this.concurrentHashMap = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T obj) {
        this.concurrentHashMap.put(obj.getEntityId(), obj);
    }

    @Override
    public boolean contains(T obj) {
        return this.concurrentHashMap.contains(obj);
    }

    @Override
    public void remove(T obj) {
        this.concurrentHashMap.remove(obj.getEntityId());
    }
}
