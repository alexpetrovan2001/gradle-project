package com.repoImplementation.fastUtil;

import com.repoInterface.InMemoryRepository;
import it.unimi.dsi.fastutil.ints.IntArrayList;

public class IntListBasedRepo implements InMemoryRepository<Integer> {
    private final IntArrayList intList;

    public IntListBasedRepo() {
        this.intList = new IntArrayList();
    }

    @Override
    public void add(Integer obj) {
        this.intList.add(obj.intValue());
    }

    @Override
    public boolean contains(Integer obj) {
        return this.intList.contains(obj.intValue());
    }

    @Override
    public void remove(Integer obj) {
        this.intList.removeInt(obj);
    }
}
