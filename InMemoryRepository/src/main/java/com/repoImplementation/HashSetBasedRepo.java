package com.repoImplementation;

import com.repoInterface.InMemoryRepository;

import java.util.HashSet;

public class HashSetBasedRepo<T> implements InMemoryRepository<T> {
    private final HashSet<T> hashSet;

    public HashSetBasedRepo(){
        this.hashSet = new HashSet<>();
    }

    @Override
    public void add(T obj) {
        this.hashSet.add(obj);
    }

    @Override
    public boolean contains(T obj) {
        return this.hashSet.contains(obj);
    }

    @Override
    public void remove(T obj) {
        this.hashSet.remove(obj);
    }
}
