package com.repoImplementation;

import com.repoInterface.InMemoryRepository;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepo<T> implements InMemoryRepository<T> {
    private final List<T> list;

    public ArrayListBasedRepo(){
        this.list = new ArrayList<>();
    }

    @Override
    public void add(T obj) {
        this.list.add(obj);
    }

    @Override
    public boolean contains(T obj) {
        return this.list.contains(obj);
    }

    @Override
    public void remove(T obj) {
        this.list.remove(obj);
    }
}
