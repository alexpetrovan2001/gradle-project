package com.repoImplementation;

import com.repoInterface.InMemoryRepository;

import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {
    private final TreeSet<T> treeSet;

    public TreeSetBasedRepository(){
        this.treeSet = new TreeSet<>();
    }

    @Override
    public void add(T obj) {
        this.treeSet.add(obj);
    }

    @Override
    public boolean contains(T obj) {
        return this.treeSet.contains(obj);
    }

    @Override
    public void remove(T obj) {
        this.treeSet.remove(obj);
    }
}
