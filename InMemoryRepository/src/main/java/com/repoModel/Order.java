package com.repoModel;

import java.util.Objects;

public class Order implements Model {
    private int id;
    private int price;
    private int quantity;

    public Order(int id, int price, int quantity){
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    public String toStr(){
        return "Order " + this.id + " - Price: " + this.price + "; Quantity: " + this.quantity;
    }

    public int getId(){
        return this.id;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    @Override
    public int hashCode(){
        return Objects.hash(id);
    }
    @Override
    public Integer getEntityId() {
        return this.id;
    }
}
