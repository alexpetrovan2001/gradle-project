plugins {
    id("java")
    id("me.champeau.jmh") version "0.7.1"
}

group = "com.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    implementation("org.eclipse.collections:eclipse-collections-api:11.0.0")
    implementation("org.eclipse.collections:eclipse-collections:11.0.0")
    implementation("it.unimi.dsi:fastutil:8.5.6")
    implementation("org.openjdk.jmh:jmh-core:1.33")
}

val jmhVersion = "1.36"

dependencies {
    implementation("org.openjdk.jmh:jmh-core:$jmhVersion")
    implementation("org.openjdk.jmh:jmh-generator-annprocess:$jmhVersion")
    annotationProcessor("org.openjdk.jmh:jmh-generator-annprocess:$jmhVersion")
}


tasks.test {
    useJUnitPlatform()
}