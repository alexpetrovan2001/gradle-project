package com.example;

import com.calculator.ui.CalculatorUI;

public class Main {
    public static void main(String[] args) {

        CalculatorUI calcUI = new CalculatorUI();

        calcUI.run();
    }
}