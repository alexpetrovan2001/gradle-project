package com.calculator.ui;

import com.calculator.Calculator;
import com.calculator.exceptions.DivisionByZeroException;

import java.util.Scanner;

public class CalculatorUI {
    private final Scanner scanner;

    private final Calculator calculator;

    public CalculatorUI(){
        this.calculator = new Calculator();

        this.scanner = new Scanner(System.in);
    }

    public void printOptions(){
        System.out.println("Calculator Menu:");
        System.out.println("1. Addition");
        System.out.println("2. Subtraction");
        System.out.println("3. Division");
        System.out.println("4. Multiplication");
        System.out.println("5. Min");
        System.out.println("6. Max");
        System.out.println("7. Square");
        System.out.println("8. Square Root");
        System.out.println("0. Exit");
    }


    private void handleSquareRoot() {
        double operand = this.inputOneNumber();

        double result = this.calculator.square(operand);

        this.printResult(result);
    }

    private void handleSquare() {
        double operand = this.inputOneNumber();

        double result = this.calculator.square(operand);

        this.printResult(result);
    }

    private void handleMax() {
        double[] operands = this.inputTwoNumbers();

        double result = this.calculator.min(operands[0], operands[1]);

        this.printResult(result);
    }

    private void handleMin() {
        double[] operands = this.inputTwoNumbers();

        double result = this.calculator.min(operands[0], operands[1]);

        this.printResult(result);
    }

    private void handleMultiplication() {
        double[] operands = this.inputTwoNumbers();

        double result = this.calculator.multiplication(operands[0], operands[1]);

        this.printResult(result);
    }

    private void handleDivision() {
        double[] operands = this.inputTwoNumbers();

        try {
            double result = this.calculator.division(operands[0], operands[1]);
            this.printResult(result);
        } catch (DivisionByZeroException e) {
            System.out.println(e.getMessage());
        }
    }

    private void handleSubtraction() {
        double[] operands = this.inputTwoNumbers();

        double result = this.calculator.subtraction(operands[0], operands[1]);

        this.printResult(result);
    }

    public void handleAddition(){

        double[] operands = this.inputTwoNumbers();

        double result = this.calculator.addition(operands[0], operands[1]);

        this.printResult(result);
    }

    public double[] inputTwoNumbers(){
        System.out.println("Enter the first number: ");
        double operand1 = scanner.nextDouble();
        System.out.println("Enter the second number: ");
        double operand2 = scanner.nextDouble();

        return new double[]{operand1, operand2};
    }

    public double inputOneNumber(){
        System.out.println("Enter a number: ");

        return scanner.nextDouble();
    }

    public void printResult(double result){
        System.out.println("Result: " + result);
    }

    public void run(){
        while(true) {
            this.printOptions();

            int option = scanner.nextInt();

            if (option == 0) {
                System.out.println("Exiting calculator .. Goodbye ! :(");
                break;
            }

            switch (option) {
                case 1:
                    this.handleAddition();
                    break;
                case 2:
                    this.handleSubtraction();
                    break;
                case 3:
                    this.handleDivision();
                    break;
                case 4:
                    this.handleMultiplication();
                    break;
                case 5:
                    this.handleMin();
                    break;
                case 6:
                    this.handleMax();
                    break;
                case 7:
                    this.handleSquare();
                    break;
                case 8:
                    this.handleSquareRoot();
                    break;
                default:
                    System.out.println("Invalid option. Please try again.");
                    break;
            }
        }
    }

}
