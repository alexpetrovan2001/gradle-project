package com.calculator;

import com.calculator.exceptions.DivisionByZeroException;

import static java.lang.Math.sqrt;

public class Calculator {

    public Calculator(){

    }

    public Double addition(Double term1, Double term2){
        return term1 + term2;
    }

    public Double subtraction(Double minuend, Double subtrahend){
        return minuend - subtrahend;
    }

    public Double multiplication(Double factor1, Double factor2){
        return factor1 * factor2;
    }

    public Double division(Double dividend, Double divisor){
        if (divisor == 0){
            throw new DivisionByZeroException("Not allowed to divide by 0 !");
        }
        return dividend / divisor;
    }

    public Double min(Double term1, Double term2){
        return Math.min(term1, term2);
    }

    public Double max(Double term1, Double term2){
        return Math.max(term1, term2);
    }

    public Double square(Double term){
        return term * term;
    }

    public Double squareRoot(Double term){
        return sqrt(term);
    }
}